#angular-loopback-professional

This will be solid template for starting enterprise level development using Loopback Framework with AngularJS.
Try to keep unnecessary stuff out. 

(Could be separated later to unopionated frontend package, AngularJS is only to have visually more awesome demo.)

Features:
- Access control with role concepts, https://github.com/strongloop/loopback-example-access-control
- Passport (social authentications), https://github.com/strongloop/loopback-example-passport
- SSL, https://github.com/strongloop/loopback-example-ssl
- Other? -> Please create issue!

Include best practices and documentation to [wiki, help wanted!](https://github.com/jykae/angular-loopback-professional/wiki/Best-Practices)

```
git clone git@github.com:jykae/angular-loopback-professional.git
cd angular-loopback-professional
npm install
slc run
```
